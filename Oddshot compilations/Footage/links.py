from bs4 import BeautifulSoup
import urllib.request, urllib.error, urllib.parse

redditFile = urllib.request.urlopen("https://oddshot.tv/t/drassel?order=top&period=week")
redditHtml = redditFile.read()
redditFile.close()

soup = BeautifulSoup(redditHtml, "lxml")
redditAll = soup.find_all("a")
for links in soup.find_all('a'):
    print('https://oddshot.tv' + (links.get('href')))