#!/bin/env python
# -*- coding: UTF-8 -*-

import sys
import urllib.parse
import re
import optparse
#from IPython.Shell import IPShellEmbed
#ipshell = IPShellEmbed()
#ipshell()

URL_SCHEME = r'^https?://'

def remove_duplicate(seq):
    """ Removes duplicates from a seq list."""
    seen = set()
    seen_add = seen.add
    return [x for x in seq if x not in seen and not seen_add(x)]

def list_optargs(option, opt, value, parser):
    """ Puts an option arguments string to a list.
        
        This function is an optparse callback function
        to return a list of option argument values
        passed as comma-separated string like "aaa, bbb".
    """
    value = re.sub(r'\s+', '', value)
    setattr(parser.values, option.dest, value.split(','))

class UrlExtractor(object):
    """ UrlExtractor class.
        UrlExtractor object does all the work of parsing and 
        is instantiated with a list of urls. 
        Optional parameters may be set via command-line to affect the output.
        
        Options:
        target_domains: A list of regex domains to be extracted.
        exclude_files: A list of regex filename extension to exclude from the result.
        keep_params: A list of query string parameter name. Named parameter's key-value
                     is kept in the resulting urls.
        maxdepth: The number of path depth (delimited by '/').
    """

    def __init__(self, urllist, target_domains=None, exclude_files=None, exclude_patterns=None, keep_params=None, maxdepth=None):
        self.domains = target_domains
        self.exclude_files = exclude_files
        self.exclude_patterns = exclude_patterns
        self.parse_results = [urllib.parse.urlparse(url) for url in urllist]
        self.parse_results_list = [list(parsed) for parsed in self.parse_results]
        self.keep_params = keep_params
        self.maxdepth = maxdepth

    def filter_by_domain(self):
        regex = "^(" + ")|(".join(self.domains) + ")"
        self.parse_results_list = [url for url in self.parse_results_list if re.match(regex, url[1])]

    def filter_by_extension(self):
        regex = "(" + ")|(".join(self.exclude_files) + ")$"
        self.parse_results_list = [url for url in self.parse_results_list if not re.search(regex, url[2])]

    def remove_port(self):
        for url in self.parse_results_list:
            url[1] = re.sub(r':[0-9]{0,5}', '', url[1])

    def handle_querystring(self):
        if self.keep_params:
            regex = "(" + "=[^&.]*|".join(self.keep_params) + "=[^&.]*)"
            for url in self.parse_results_list:
                url[4] = ''.join(re.findall(regex, url[4]))
        else:
            for url in self.parse_results_list:
                url[4] = ''

    def reduce_path(self):
        if self.maxdepth is not None:
            maxdepth = int(self.maxdepth)
            for url in self.parse_results_list:
                paths = url[2].split('/')
                if len(paths) > maxdepth:
                    paths = paths[0:maxdepth]
                    url[2] = '/'.join(paths)

    def filter_by_pattern(self):
        regex = "(" + ")|(".join(self.exclude_patterns) + ")"
        self.parse_results_list = [url for url in self.parse_results_list if not re.search(regex, str(url))]
    
if __name__ == '__main__':
    # parse options
    parser = optparse.OptionParser(
            usage='Usage: %prog [options] urlfile',
            version='2011.08.13',
            conflict_handler='resolve'
    )

    parser.add_option('-h', '--help', action='help', help='print help text')
    parser.add_option('-d', '--domain', type='string', action='callback', callback=list_optargs, dest='target_domains', help='Domain(s) to include. Comma-separate multiple domains like "1.com, 2.com".')
    parser.add_option('-p', '--port', action='store_true', dest='port_flag', default=False, help='Use if port number should be kept in the resulting url. Defaults to False.')
    parser.add_option('-x', '--exclude', type='string', action='callback', callback=list_optargs, dest='exclude_files', help='File extensions to exclude. Comma-separate multiple file extensions like "css, jpg".')
    parser.add_option('-X', '--filter', type='string', action='callback', callback=list_optargs, dest='exclude_strings', help='String pattern to exclude. Comma-separate multiple strings like "aaa, bbb"')
    parser.add_option('-m', '--maxdepth', type='string', action='store', dest='maxdepth', help='Maxdepth of the path (slash delimited) to preserve')
    parser.add_option('-k', '--key', type='string', action='callback', callback=list_optargs, dest='keep_params', help='Query string parameter(s) to preserve. Comma-separate multiple keys like "action, act".')
    (opts, args) = parser.parse_args()

    #print opts

    # Missing argument
    if len(args) == 0:
        parser.error('urlfile is not specified. For available options, try -h or --help.')

    # Make list of urls from input file
    try:
        source_file = open(args[0], 'r')
    except IOError:
        print("ERROR: cannot open the file", args[0])
        sys.exit()

    source_urls = [url.strip() for url in source_file.readlines() if url.strip() and re.match(URL_SCHEME, url) is not None]
 
    # Instantiating UrlExtractor
    urlex = UrlExtractor(source_urls, opts.target_domains, opts.exclude_files, opts.exclude_strings, opts.keep_params, opts.maxdepth)

    # Filtering by domain, if speficied
    if opts.target_domains is not None:
        urlex.filter_by_domain()

    # Filtering out specified file extension(s)
    if opts.exclude_files is not None:
        urlex.filter_by_extension()

    # Filters out specified strings
    if opts.exclude_strings is not None:
        urlex.filter_by_pattern()

    # Removing port from netloc
    if opts.port_flag is True:
        urlex.remove_port()

    # Querystring processing
    urlex.handle_querystring()

    # Path depth
    if opts.maxdepth is not None:
        urlex.reduce_path()

    # Output to stdout after removing duplicate element
    fin_list = remove_duplicate([urllib.parse.urlunparse(url) for url in urlex.parse_results_list])
for u in fin_list: print(u)